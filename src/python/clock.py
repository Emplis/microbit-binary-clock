from enum import Enum

MICRO_BIT = False

if MICRO_BIT:
    from microbit import *
else:
    import time

def m_clear_pixel():
    if MICRO_BIT:
        display.clear()
    return

def m_set_pixel(x, y, brightness):
    if MICRO_BIT:
        display.set_pixel(x, y, brightness)
    return

def m_sleep(msec):
    if MICRO_BIT:
        sleep(msec)
    else:
        time.sleep(msec // 1000)

def m_pin_logo_is_touched():
    if MICRO_BIT:
        return pin_logo.is_touched()
    return False

def m_btn_a_was_pressed():
    if MICRO_BIT:
        return button_a.was_pressed()
    return False

def m_btn_b_was_pressed():
    if MICRO_BIT:
        return button_b.was_pressed()
    return False

class EditStep(Enum):
    SECONDS
    MINUTES
    HOURS

def getBit(integer, n):
    assert integer >= 0
    return ((integer >> n) & 1)

def getMinutes(seconds):
    if seconds >= 60:
        minutes = seconds // 60
        remaining_sec = seconds % 60
        return (minutes, remaining_sec)
    else:
        return (0, seconds)

def getHours(seconds):
    if seconds >= 3600:
        hours = seconds // 3600
        remaining_sec = seconds % 3600
        return (hours, remaining_sec)
    else:
        return (0, seconds)

def printSeconds(seconds):
    assert (seconds >= 0 and seconds <= 59)

    digit1 = seconds % 10 # max value of digit1 is 9 so 4 bits
    digit2 = seconds // 10 # max value of digit2 is 5 so 3 bits

    d1X = 4
    d2X = 3

    if getBit(digit1, 0) == 1:
        m_set_pixel(d1X, 4, 9)
    if getBit(digit1, 1) == 1:
        m_set_pixel(d1X, 3, 9)
    if getBit(digit1, 2) == 1:
        m_set_pixel(d1X, 2, 9)
    if getBit(digit1, 3) == 1:
        m_set_pixel(d1X, 1, 9)

    if getBit(digit2, 0) == 1:
        m_set_pixel(d2X, 4, 9)
    if getBit(digit2, 1) == 1:
        m_set_pixel(d2X, 3, 9)
    if getBit(digit2, 2) == 1:
        m_set_pixel(d2X, 2, 9)

def printMinutes(minutes):
    assert (minutes >= 0 and minutes <= 59)

    digit1 = minutes % 10 # max value of digit1 is 9 so 4 bits
    digit2 = minutes // 10 # max value of digit2 is 5 so 3 bits

    d1X = 2
    d2X = 1

    if getBit(digit1, 0) == 1:
        m_set_pixel(d1X, 4, 9)
    if getBit(digit1, 1) == 1:
        m_set_pixel(d1X, 3, 9)
    if getBit(digit1, 2) == 1:
        m_set_pixel(d1X, 2, 9)
    if getBit(digit1, 3) == 1:
        m_set_pixel(d1X, 1, 9)

    if getBit(digit2, 0) == 1:
        m_set_pixel(d2X, 4, 9)
    if getBit(digit2, 1) == 1:
        m_set_pixel(d2X, 3, 9)
    if getBit(digit2, 2) == 1:
        m_set_pixel(d2X, 2, 9)

def printHours(hours):
    assert (hours >= 0 and hours <= 23)

    x = 0

    if getBit(hours, 0) == 1:
        m_set_pixel(x, 4, 9)
    if getBit(hours, 0) == 1:
        m_set_pixel(x, 3, 9)
    if getBit(hours, 0) == 1:
        m_set_pixel(x, 2, 9)
    if getBit(hours, 0) == 1:
        m_set_pixel(x, 1, 9)
    if getBit(hours, 0) == 1:
        m_set_pixel(x, 0, 9)

def printClock(hours, minutes, seconds):
    m_clear_pixel()
    print(f"\rh:{hours:02d}; m:{minutes:02d}; s:{seconds:02d}", end='')
    printSeconds(seconds)
    printMinutes(minutes)
    printHours(hours)

def cycle_seconds(seconds, clockwise):
    pass

def cycle_minutes(seconds, clockwise):
    pass

def cycle_hours(seconds, clockwise):
    res = seconds
    (hours, _) = getHours(seconds)
    if clockwise: # from 0 to 23
        if hours == 23:
            res = seconds - (3600 * 23)
        else:
            res = seconds + 3600
    else: # from 23 to 0
        if hours == 0:
            res = seconds + (3600 * 23)
        else:
            res = seconds - 3600

    return res

def main():
    time_counter = 0
    edit_time = False
    edit_step = EditStep.SECONDS

    while True:
        if edit_time:
            match edit_step:
                case SECONDS:
                    if m_pin_logo_is_touched():
                        edit_step = EditStep.MINUTES
                    if m_btn_a_was_pressed():
                        time_counter = cycle_seconds(time_counter, clockwise=False)
                    if m_btn_b_was_pressed():
                        time_counter = cycle_seconds(time_counter, clockwise=True)
                case MINUTES:
                    if m_pin_logo_is_touched():
                        edit_step = EditStep.HOURS
                    if m_btn_a_was_pressed():
                        time_counter = cycle_minutes(time_counter, clockwise=False)
                    if m_btn_b_was_pressed():
                        time_counter = cycle_minutes(time_counter, clockwise=True)
                case HOURS:
                    if m_pin_logo_is_touched():
                        edit_step = EditStep.SECONDS
                        edit_step = False
                    if m_btn_a_was_pressed():
                        time_counter = cycle_hours(time_counter, clockwise=False)
                    if m_btn_b_was_pressed():
                        time_counter = cycle_hours(time_counter, clockwise=True)
            m_sleep(100)
        else:
            if m_pin_logo_is_touched():
                edit_time = True

            m_sleep(1000)

            time_counter += 1

        # print(time_counter)
        (hours, remaining_sec) = getHours(time_counter)
        (minutes, seconds) = getMinutes(remaining_sec)
        printClock(hours, minutes, seconds)

if __name__ == "__main__":
    main()
