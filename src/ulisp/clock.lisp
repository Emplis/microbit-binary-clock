(defun getBit (integer n)
    (logbitp n integer))

(defun getMinutes (seconds)
    (if (>= seconds 60)
        (let ((minutes (truncate seconds 60))
              (remaining_sec (mod seconds 60)))
            (return (list minutes remaining_sec)))
        (list 0 seconds)))

(defun getHours (seconds)
    (if (>= seconds 3600)
        (let ((hours (truncate seconds 3600))
              (remaining_sec (mod seconds 3600)))
              (return (list hours remaining_sec)))
        (list 0 seconds)))

(defun setPixel (x y on)
    (let ((_y (+ y 21))
          (_x (case x
                (0 4)
                (1 7)
                (2 3)
                (3 6)
                (4 10))))
         (pinmode _x :output)
         (pinmode _y :output)
         (digitalwrite _x (not on))
         (digitalwrite _y on)
         (list _x _y)))

(defun resetDisplay
    (dotimes (x 5)
        (dotimes (y 5)
            (setPixel x y nil))))

(defun printSeconds (seconds)
    (let ((digit1 (mod seconds 10))
          (digit2 (truncate seconds 10))
          (d1X 10)
          (d2X 6))
         (pinmode d1X :output)
         (digitalwrite d1X nil)
         (if (= (getBit digit1 0) 1)
             ((pinmode 25 :output)
              (digitalwrite 25 t)))))
